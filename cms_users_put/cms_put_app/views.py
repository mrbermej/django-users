from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth import logout
from .models import Contenido, Comentario

# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        action = request.POST['action']
        if action == "Enviar Contenido":
            valor = request.POST['valor']
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    if request.method == "POST" and action == "Enviar Comentario":
            c = get_object_or_404(Contenido, clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
            q.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    autenticado = request.user.is_authenticated
    usuario = request.user.username
    context = {'contenido': contenido, 'autenticado':autenticado, 'user': usuario}
    return render(request, 'cms_put_app/content.html', context)

def index(request):
    content_list = Contenido.objects.all()#[:5]
    autenticado = request.user.is_authenticated
    context = {'content_list': content_list, 'autenticado':autenticado}
    return render(request, 'cms_put_app/index.html', context)


#esta función comprueba si se está registrado o no, si no está registrado redirecciona a la página de admin para registrarte
def logged_in(request):
    if request.user.is_authenticated:
        respuesta = "Bienvenido, te has registrado como " + "<h1>" + request.user.username + "</h1>"
    else:
        respuesta = "No esta registrado, por favor <a href='/admin/'>inicia sesion</a>"
    return HttpResponse(respuesta)

def logged_out(request):
    logout(request)
    return redirect("/cms/") #esto redirecciona a la plantilla dentro de cms que la podemos llamar index o como queramos

