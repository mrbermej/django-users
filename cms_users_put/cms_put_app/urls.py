from django.urls import path
from .views import index, get_content, logged_in, logged_out

urlpatterns = [
    path("", index),
    path("login", logged_in),
    path("logout", logged_out),
    path('<str:llave>', get_content)
]