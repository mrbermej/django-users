from django.db import models

class Contenido(models.Model):
    clave = models.CharField(max_length=100)
    valor = models.CharField(max_length=48)

    def __str__(self):
        return f"{self.id} --> {self.clave} --> {self.valor}"

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    comentario = models.TextField(blank=False)
    fecha = models.DateTimeField('publicado')

    def __str__(self):
        return f"{self.id}: {self.titulo} --- {self.contenido.clave}"
